#Provider Block
provider "aws" {
region = "us-west-2"
}
#Create a VPC
resource "aws_vpc" "vpc-dev" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "vpc-dev"
  }
}

#Create a public subnet 
resource "aws_subnet" "pulic-subnet-1" {
  vpc_id     = aws_vpc.vpc-dev.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-west-2c"
  map_public_ip_on_launch = true
  tags = {
    Name = "vpc-dev-public-subnet"
  }
}
#Create Security Group
resource "aws_security_group" "dev-sg" {
  name        = "dev-sg"
  description = "Dev VPC Default Security Group"
  vpc_id      = aws_vpc.vpc-dev.id

  ingress {
    description      = "Allow Port 22"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }
  ingress {
    description      = "Allow Port 80"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
   
  }
  egress {
    description = "Allow all IP and Ports Outbound"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
# Create EC2 Instance
resource "aws_instance" "my-instance" {
  ami = "ami-017c629ce8712b5d4" # us-east-1
  instance_type = "t2.micro"
  key_name = "kodemade-aws"
  subnet_id = aws_subnet.pulic-subnet-1.id
  vpc_security_group_ids = [aws_security_group.dev-sg.id]

}
